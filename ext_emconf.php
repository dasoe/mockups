<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'mockups',
    'description' => '',
    'category' => 'plugin',
    'author' => 'das oe',
    'author_email' => 'christian.oettinger@gmx.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
